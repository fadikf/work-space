//
//  WSBrain.h
//  SpaceConnect
//
//  Created by Fadi Kfoury on 11/29/14.
//  Copyright (c) 2014 WorkSpace. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WSBrain : NSObject
{

}

@property (nonatomic,strong) NSString * userName;
@property (nonatomic,strong) NSString * passWord;
@property (nonatomic,weak)  UIViewController   * loginController;
@property (nonatomic,weak) NSString * apnsToken;


-(NSDictionary*) returnNearbyWorkSpace;


-(void) attemptFirstLoginFBlock:( void(^) (NSString*error)) fBlock andSblock:(void(^)(NSDictionary*response))sBlock;

-(void) loginWithEmail:(NSString*) email andPassword:(NSString*) password  andFBlock:( void(^) (NSString*error)) fBlock andSblock:(void(^)(NSDictionary*response))sBlock;
-(void) redirectAndAttemptFirstLogIn ;

-(void) logOutAndClearUserCredentials;

-(void) registerWith:(NSString*) email andPassword:(NSString*) password  andAbout:(NSString*) aboutString ansMac:(NSString*) MAC andFBlock:( void(^) (NSString*error)) fBlock andSblock:(void(^)(NSDictionary*response))sBlock;


-(void) getUserSpaceWithFBlock:( void(^) (NSString*error)) fBlock andSblock:(void(^)(NSDictionary*response))sBlock;


 @end
