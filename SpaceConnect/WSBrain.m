//
//  WSBrain.m
//  SpaceConnect
//
//  Created by Fadi Kfoury on 11/29/14.
//  Copyright (c) 2014 WorkSpace. All rights reserved.
//

#import "WSBrain.h"
#import "ViewController.h"


@implementation WSBrain

{
    

    void (^response)(NSDictionary * response);
    void (^failBlock)(NSString*error);
    
    NSURLConnection * myConnection;
    NSDictionary * xBodyRequested;
    
    BOOL credentialsSaved;
    
   
    
    

    
}

@synthesize loginController,apnsToken;



-(void) logOutAndClearUserCredentials
{
    NSUserDefaults * myUserDefaults = [NSUserDefaults standardUserDefaults];
    [myUserDefaults setObject:@"" forKey:@"username"];
    [myUserDefaults setObject:@"" forKey:@"password"];
    
    [myUserDefaults synchronize];
    
    [[self loginController] dismissViewControllerAnimated:YES completion:^{
        
    }];
    
    
}

-(void) attemptFirstLoginFBlock:( void(^) (NSString*error)) fBlock andSblock:(void(^)(NSDictionary*response))sBlock
{
    
    //loads the essentials user name and password credentials
    NSUserDefaults * myUserDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString * username = [myUserDefaults objectForKey:@"username"];
    NSString * password = [myUserDefaults objectForKey:@"password"];
    
    if (username.length != 0 && password.length != 0)
    {
        
        [self loginWithEmail:username andPassword:password andFBlock:^(NSString *error)
        {
            
            //Authnetication failed
            //wipe credentials
            
            fBlock(@"failed");
            
            
        } andSblock:^(NSDictionary *response)
        {
            //authentication succesfull should r
            sBlock(response);
            
            
            
            
        }];
        
    }else
        
    {
        fBlock(@"1");
    }
    
    

    
    
}

-(void) saveUserCredentialsUserNameAnd:(NSString*) userName andPassword:(NSString*) password
{
    NSUserDefaults * myUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [myUserDefaults setObject:userName forKey:@"username"];
    [myUserDefaults setObject:password forKey:@"password"];
    
    [myUserDefaults synchronize];
    
    
    
}

-(void) redirectAndAttemptFirstLogIn //used by login and register view controllers
{
    [loginController.navigationController popToRootViewControllerAnimated:YES];
   
    
    [(ViewController*)loginController  attemptLogin];
    
    
    
}

-(void) loginWithEmail:(NSString*) email andPassword:(NSString*) password  andFBlock:( void(^) (NSString*error)) fBlock andSblock:(void(^)(NSDictionary*response))sBlock
{
   
    
    NSMutableDictionary * myDictionary = [NSMutableDictionary new];
    
    if (email.length != 0 && password.length != 0)
    {
        
    [myDictionary setObject:email forKey:@"email"];
    [myDictionary setObject:password forKey:@"password"];
    [self initialzeRequestWithURLPath:@"auth" andXbody:myDictionary andFBlock:^(NSString *error)
    {
        
        fBlock(error);
        
        
    } andSblock:^(NSDictionary *response)
    {
        if ([[response objectForKey:@"response_number"] isEqualToString:@
             "1"]) {
            
            [self saveUserCredentialsUserNameAnd:email andPassword:password];
            
        
            
            sBlock(response);
            
            
        }else
        {
            fBlock(@"0");
            
        }
        
    }];
        
        
    }
    else
    {
        fBlock(@"1"); //missing entries
    }
    
    
}





-(void) getUserSpaceWithFBlock:( void(^) (NSString*error)) fBlock andSblock:(void(^)(NSDictionary*response))sBlock
{
    
    
    NSMutableDictionary * myDictionary = [NSMutableDictionary new];
    
    
        
    [myDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"username"] forKey:@"email"];
    [myDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"password"] forKey:@"password"];
    
        
        [self initialzeRequestWithURLPath:@"get_users" andXbody:myDictionary andFBlock:^(NSString *error)
         {
             
             fBlock(@"0");
             
             
         } andSblock:^(NSDictionary *response)
         {
             
             //if response was ok proceed , otherwise
             
             
             
             
            
                        // 1st Key : listOfDevices
                 
                        // 2nd Key : workSpace
                 
                 sBlock(response);
                 
                 
             
             
             
         }];
        
        
    }

    
    



-(void) registerWith:(NSString*) email andPassword:(NSString*) password  andAbout:(NSString*) aboutString ansMac:(NSString*) MAC andFBlock:( void(^) (NSString*error)) fBlock andSblock:(void(^)(NSDictionary*response))sBlock
{
    
    
    NSMutableDictionary * myDictionary = [NSMutableDictionary new];
    
    
    if (email.length != 0 && password.length != 0 && MAC.length == 17 && apnsToken.length == 64 )
    {
        
        [myDictionary setObject:email forKey:@"email"];
        [myDictionary setObject:password forKey:@"password"];
        [myDictionary setObject:MAC forKey:@"MAC"];
        [myDictionary setObject:apnsToken  forKey:@"APNS"];
        [myDictionary setObject:aboutString forKey:@"about"];
        
        [self initialzeRequestWithURLPath:@"registerUser" andXbody:myDictionary andFBlock:^(NSString *error)
         {
             fBlock(@"0");
             
             
         } andSblock:^(NSDictionary *response)
         {
             
             //if response was ok proceed , otherwise
             
             
             
             
             if ([[response objectForKey:@"response_number"] isEqualToString:@"1"])
                 
             {
                 
                 [self saveUserCredentialsUserNameAnd:email andPassword:password];
                 
                 sBlock(response);
                 
                 
             }
             
             else
             {
                 fBlock(@"1");
             }
         }];
        
        
    }
    else
    {
        fBlock(@"1"); //missing entries
    }
    
    
}



-(void) initialzeRequestWithURLPath:(NSString*) str andXbody:(NSDictionary*) dictionary  andFBlock:( void(^) (NSString*error)) fBlock andSblock:(void(^)(NSDictionary*response))sBlock
{
    
    
    NSString * baseURL = @"http://fadikf.pythonanywhere.com";
    
    NSURL * url = [NSURL URLWithString: [baseURL stringByAppendingPathComponent:str]];
    
    NSError * myErrro;
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: url];
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"charset" forHTTPHeaderField:@"utf-8"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
   
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&myErrro];
    
 
    NSString * stringOfDataToOutput =   [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&myErrro] encoding:NSUTF8StringEncoding  ];
    
    
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         
         if (error != nil)
         {
             fBlock(@"-1");
         }else
         {
             
         
         
             if([data length] >= 1)
             {
                 
                 
                 NSDictionary * responseDictionary = [NSJSONSerialization JSONObjectWithData: data options: 0 error: nil];
                 // Do computations here
                 sBlock(responseDictionary);
                 
                 
             }else
             {
                 fBlock(@"0");
             }
             
             
         }
         
         
     }];
    
    
}
@end
