//
//  WSLoginController.h
//  SpaceConnect
//
//  Created by Fadi Kfoury on 11/29/14.
//  Copyright (c) 2014 WorkSpace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface WSLoginController : UIViewController
{
    WSBrain * myBrain;
    
}
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *cmdLoginOutlet;
- (IBAction)cmdLogin:(id)sender;


@end
