//
//  WSWorkSpaceController.m
//  SpaceConnect
//
//  Created by Fadi Kfoury on 11/29/14.
//  Copyright (c) 2014 WorkSpace. All rights reserved.
//

#import "WSWorkSpaceController.h"
#import <MessageUI/MessageUI.h>

@implementation WSWorkSpaceController
{
    WSBrain * myBrain;
    
}
@synthesize cmdLogoutOutlet,cmdSpaceEmailOutlet,tableView,lblWorkSpace,cmdRefreshO;

-(void) viewDidLoad
{
    [super viewDidLoad];
    
    myBrain = [(AppDelegate*)[[UIApplication sharedApplication] delegate] theBrain];
    [self refreshWorkSpace];
    
    
    
}
- (IBAction)CMDspaceEmail:(id)sender {
    if (dataSource.count != 0)
    [self sendEmailtoRecipents:dataSource];
}


- (IBAction)cmdLogOut:(id)sender {
    [myBrain logOutAndClearUserCredentials];
     
}

-(void) refreshWorkSpace
{
    
    [myBrain  getUserSpaceWithFBlock:^(NSString *error)
    {
        dataSource = [NSMutableArray new];
        
        
      
            dispatch_async(dispatch_get_main_queue(),
            ^{
                 [tableView reloadData];
                 lblWorkSpace.text = @"No Work Spaces Around";
            });
        
        
            
        
    } andSblock:^(NSDictionary *response)
     {
         dataSource = [response objectForKey:@"users"];
         nameOfConenctedWorkStation = [response objectForKey:@"work station"];
         [tableView reloadData];
         if ([[nameOfConenctedWorkStation lowercaseString ] isEqualToString:@"No Work Spaces Around"])
         {
              lblWorkSpace.text = @"No Work Spaces Around";
         }
        
         else
         {
             lblWorkSpace.text =[NSString stringWithFormat:@"You are connected to %@", nameOfConenctedWorkStation];
             
         }
         
    }];
    
    
}


- (IBAction)cmdSpaceEmail:(id)sender
{
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (dataSource.count != 0) {
        [self sendEmailtoRecipents:[NSArray arrayWithObject:[dataSource objectAtIndex:indexPath.row]]];
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (dataSource.count == 0) {
        return 1;
    }else
    {
    return dataSource.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        NSString *simpleTableIdentifier;
    
    if (dataSource.count == 0)
        {
            simpleTableIdentifier = @"noOneAround";
    
        }
        else
        {
            simpleTableIdentifier = @"noOneAround";
        }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
        if (dataSource.count != 0)
        {
    
cell.textLabel.text = [dataSource objectAtIndex:indexPath.row];
            
        }
    return cell;
    
}



- (IBAction)cmdRefresh:(id)sender {
    [self refreshWorkSpace];
    
}


- (void) sendEmailtoRecipents:(NSArray*) toRecipents {
    // Email Subject
    NSString *emailTitle = @"Space Connect App Email";
    // Email Content
    NSString *messageBody = @"         -Sent from Space Connect App";
    // To address
 // NSArray *toRecipents = [NSArray arrayWithObject:@"support@appcoda.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    switch ((int)result)
    {
        case (int)MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case (int)MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case (int)MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case (int)MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
