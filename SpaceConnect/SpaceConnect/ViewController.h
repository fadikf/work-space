//
//  ViewController.h
//  SpaceConnect
//
//  Created by Fadi Kfoury on 11/29/14.
//  Copyright (c) 2014 WorkSpace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"




@interface ViewController : UIViewController <WSLoginProtocol>
{
    WSBrain * myBrain;
    
}
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *myActivity;
@property (weak, nonatomic) IBOutlet UIButton *cmdLogin;
@property (weak, nonatomic) IBOutlet UIButton *cmdRegister;


@end

