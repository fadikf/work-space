//
//  WSWorkSpaceController.h
//  SpaceConnect
//
//  Created by Fadi Kfoury on 11/29/14.
//  Copyright (c) 2014 WorkSpace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


@interface WSWorkSpaceController : UIViewController <UITableViewDataSource,UITableViewDelegate>

{
    NSMutableArray * dataSource;
    NSString * nameOfConenctedWorkStation;
    
}
- (IBAction)cmdLogOut:(id)sender;
- (IBAction)cmdLogout:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *cmdLogoutOutlet;
- (IBAction)cmdSpaceEmail:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *cmdSpaceEmailOutlet;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblWorkSpace;
- (IBAction)cmdRefresh:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *cmdRefreshO;


@end
