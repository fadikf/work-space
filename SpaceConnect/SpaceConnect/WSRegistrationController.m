//
//  WSRegistrationController.m
//  SpaceConnect
//
//  Created by Fadi Kfoury on 11/29/14.
//  Copyright (c) 2014 WorkSpace. All rights reserved.
//

#import "WSRegistrationController.h"
#import "AppDelegate.h"

@implementation WSRegistrationController
{
    UIGestureRecognizer * tapper;
    WSBrain * theBrain;
    
    
}
@synthesize txtPassword,txtEmail,txtAboutYou,txtMACAddress,cmdRegisterOutlet;


-(void) viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    
    tapper = [[UITapGestureRecognizer alloc]
              
              initWithTarget:self action:@selector(handleSingleTap:)];
    
    tapper.cancelsTouchesInView = NO;
    
    [self.view addGestureRecognizer:tapper];
    
    theBrain = [(AppDelegate*)[[UIApplication sharedApplication] delegate] theBrain];
    
    
   
    
}


- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}



#define kOFFSET_FOR_KEYBOARD 80.0

-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide {
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
   
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


- (IBAction)cmdRegister:(id)sender
{
    
    [theBrain registerWith:txtEmail.text andPassword:txtPassword.text andAbout:txtAboutYou.text ansMac:txtMACAddress.text andFBlock:^(NSString *error) {
        NSLog(@"Registration Failed");
    } andSblock:^(NSDictionary *response) {
        [theBrain redirectAndAttemptFirstLogIn];
        
    }];
}


@end
