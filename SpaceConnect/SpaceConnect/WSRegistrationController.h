//
//  WSRegistrationController.h
//  SpaceConnect
//
//  Created by Fadi Kfoury on 11/29/14.
//  Copyright (c) 2014 WorkSpace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WSRegistrationController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;

@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtMACAddress;

@property (weak, nonatomic) IBOutlet UITextField *txtAboutYou;

@property (weak, nonatomic) IBOutlet UIButton *cmdRegisterOutlet;
- (IBAction)cmdRegister:(id)sender;

@end
