//
//  ViewController.m
//  SpaceConnect
//
//  Created by Fadi Kfoury on 11/29/14.
//  Copyright (c) 2014 WorkSpace. All rights reserved.
//

#import "ViewController.h"
#import <SpinKit/RTSpinKitView.h>
@interface ViewController ()
{
    RTSpinKitView *spinner;
    
}
@end





@implementation ViewController
@synthesize myActivity,cmdLogin,cmdRegister;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    AppDelegate * myDelelgate = [[UIApplication sharedApplication] delegate];
    
    myBrain = [myDelelgate theBrain];
    [myBrain setLoginController:self];
    
    
    // Do any additional setup after loading the view, typically from a nib.
    
   // [myActivity startAnimating];

    CGRect screenBounds = [self.view frame];
    


spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:[UIColor whiteColor]];
    spinner.spinnerSize = 100.0;
    [spinner sizeToFit];
    spinner.center = CGPointMake(CGRectGetMidX(screenBounds), CGRectGetMidY(screenBounds));
    
  
    
    
    [spinner startAnimating];
    [self.view addSubview:spinner];
    
    [self attemptLogin];
    
    
    
}


-(void) attemptLogin
{
    
    [cmdRegister setAlpha:0];
    [cmdLogin setAlpha:0];
    
    [myBrain attemptFirstLoginFBlock:^(NSString *error)
    {
        
        
        [cmdRegister setAlpha:1];
        [cmdLogin setAlpha:1];
        NSLog(@"FAILED TO LOGIN");
        
        
    } andSblock:^(NSDictionary *response)
    {
        
        [self performSegueWithIdentifier:@"enterWorkSpace" sender:self];
        
        [cmdRegister setAlpha:1];
        [cmdLogin setAlpha:1];
        
    }];
    
}

-(void) viewDidAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ///[spinner removeFromSuperview];
    
}

@end
