//
//  AppDelegate.h
//  SpaceConnect
//
//  Created by Fadi Kfoury on 11/29/14.
//  Copyright (c) 2014 WorkSpace. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSBrain.h"

@protocol WSLoginProtocol <NSObject>

-(void) attemptLogin;


@end



@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) WSBrain * theBrain;

@end

