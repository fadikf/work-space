//
//  WSLoginController.m
//  SpaceConnect
//
//  Created by Fadi Kfoury on 11/29/14.
//  Copyright (c) 2014 WorkSpace. All rights reserved.
//

#import "WSLoginController.h"

@implementation WSLoginController


{
    UIGestureRecognizer * tapper;
    
}

@synthesize cmdLoginOutlet,txtEmail,txtPassword;


-(void) viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    tapper = [[UITapGestureRecognizer alloc]
              
              initWithTarget:self action:@selector(handleSingleTap:)];
    
    tapper.cancelsTouchesInView = NO;
    
    [self.view addGestureRecognizer:tapper];
    
    myBrain = [(AppDelegate*)[[UIApplication sharedApplication] delegate] theBrain];
    

    
    
}


- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}







- (IBAction)cmdLogin:(id)sender

{
    
    [myBrain loginWithEmail:txtEmail.text andPassword:txtPassword.text andFBlock:^(NSString *error) {
        NSLog(@"Failed to login");
    } andSblock:^(NSDictionary *response) {
        [myBrain redirectAndAttemptFirstLogIn];
        
    }];
}
@end
